#!/home/kali/ITS/network-programs/env/bin/python
"""
Exercise 3: Use urllib to replicate the previous exercise of (1) retrieving
the document from a URL, (2) displaying up to 3000 characters, and
(3) counting the overall number of characters in the document. Don’t
worry about the headers for this exercise, simply show the first 3000
characters of the document contents.
"""

import urllib.request

url = input('Enter a url: ')
if url == "":
    url = 'https://www.w3.org/Protocols/rfc2616/rfc2616.txt'

fhand = urllib.request.urlopen(url)
char_count = 0
chars_printed = 0

for line in fhand:
    line_split = line.decode().split()

    if not line_split:
        continue
    else:
        for item in line_split:
            char_count += (len(item))

    if char_count <= 3000:
        print(line.decode().strip())
        chars_printed = char_count

print(f'chars received: {char_count}, chars printed {chars_printed}')
