#!/home/kali/ITS/network-programs/env/bin/python
"""
Exercise 2: Change your socket program so that it counts the number
of characters it has received and stops displaying any text after it has
shown 3000 characters. The program should retrieve the entire docu-
ment and count the total number of characters and display the count
of the number of characters at the end of the document.

test with https://www.w3.org/Protocols/rfc2616/rfc2616.txt
"""

import socket

mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# input url, use default if nothing entered
url = input('Enter a url: ')
if url == "":
    url = 'https://www.w3.org/Protocols/rfc2616/rfc2616.txt'

# extract domain from url
domain = url.split("//")[-1].split("/")[0].split('?')[0]

# connect a socket on port 80 to the domain
mysock.connect((domain, 80))

# build and encode the http GET command
cmd = 'GET ' + url + ' HTTP/1.0\r\n\r\n'
cmd_encoded = cmd.encode()

# send the byte encoded GET command
mysock.send(cmd_encoded)

# receive 512 chars at a time, decode and print until empty line received
received = []
count = 0
chars_printed = 0

while True:
    data = mysock.recv(500)
    received.append(data.decode())
    if len(data) < 1:
        break
    for item in received:
        count += len(item)
        if count <= 3000:
            print(item)
            chars_printed = count

print(f'chars received {count}, chars printed {chars_printed}')

# close socket
mysock.close()
