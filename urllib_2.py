#!/home/kali/ITS/network-programs/env/bin/python
"""
Exercise 4: Change the urllinks.py program to extract and count para-
graph (p) tags from the retrieved HTML document and display the
count of the paragraphs as the output of your program. Do not display
the paragraph text, only count them. Test your program on several
small web pages as well as some larger web pages.

http://www.dr-chuck.com/page1.htm
https://www.ucl.dk/
"""

from urllib.request import Request, urlopen
import urllib.error
from bs4 import BeautifulSoup
import ssl

# Ignore SSL certificate errors
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

url = input('Enter a url: ')
if url == "":
    url = 'https://docs.python.org'

# https://medium.com/@raiyanquaium/how-to-web-scrape-using-beautiful-soup-in-python-without-running-into-http-error-403-554875e5abed
req = Request(url, headers={'User-agent': 'Mozilla/5.0'})

html = urllib.request.urlopen(req, context=ctx,).read()
soup = BeautifulSoup(html, 'html.parser')

# Retrieve all of the anchor tags
count = 0
#tags = soup('body')
tags = soup.find_all('p')
count = len(tags)
# for tag in tags:
#     count += 1

print(f'found {count} p tags @ {url}')
